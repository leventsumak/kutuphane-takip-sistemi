﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KutuphaneTS.Models.ORM
{
    public class KitapModel
    {
        public int KitapID { get; set; }
        [Display(Name = "Kitap Adı")]
        public string KitapAdi { get; set; }
        [Display(Name = "Yazar")]
        public string Yazar { get; set; }
        [Display(Name = "Raf No")]
        public string RafNo { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Basım Tarihi")]
        public Nullable<System.DateTime> BasimTarihi { get; set; }
        public Nullable<int> KutuphanedeMi { get; set; }
    }
}