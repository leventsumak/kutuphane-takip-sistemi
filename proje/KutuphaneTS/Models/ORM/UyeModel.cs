﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KutuphaneTS.Models.ORM
{
    public class UyeModel
    {
        public int KullaniciID { get; set; }
        [Display(Name = "EPosta")]
        public string EPosta { get; set; }
        public string Sifre { get; set; }
        [Display(Name = "Adı")]
        public string Adi { get; set; }
        [Display(Name = "Soyadı")]
        public string Soyadi { get; set; }
        public Nullable<int> Yetki { get; set; }
    }
}