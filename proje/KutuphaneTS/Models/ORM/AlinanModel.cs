﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KutuphaneTS.Models.ORM
{
    public class AlinanModel
    {
        public int AlinanID { get; set; }
        public Nullable<int> KullaniciID { get; set; }
        public Nullable<int> KitapID { get; set; }
        [Display(Name = "Alınma Tarihi")]
        public Nullable<System.DateTime> AlinmaTarihi { get; set; }
    }
}