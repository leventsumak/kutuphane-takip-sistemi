//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KutuphaneTS.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class kullanicilar
    {
        public int KullaniciID { get; set; }
        public string EPosta { get; set; }
        public string Sifre { get; set; }
        public string Adi { get; set; }
        public string Soyadi { get; set; }
        public Nullable<int> Yetki { get; set; }
    }
}
