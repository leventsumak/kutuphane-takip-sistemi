﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KutuphaneTS.Models.SessionControl
{
    public class AdminLoginControl : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            if (httpContext.Session["loginAdmin"] != null)
            {
                httpContext.Response.Redirect("/Admin");
                return false;
            }

            return true;

        }
    }
}