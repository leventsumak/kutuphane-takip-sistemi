﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KutuphaneTS.Models.SessionControl
{
    public class UserControl : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            if (httpContext.Session["loginUser"] == null)
            {
                httpContext.Response.Redirect("/UserLogin");
                return false;
            }
            return true;

        }
    }
}