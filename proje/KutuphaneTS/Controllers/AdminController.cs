﻿using KutuphaneTS.Models.DB;
using KutuphaneTS.Models.ORM;
using KutuphaneTS.Models.SessionControl;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KutuphaneTS.Controllers
{
    [AdminControl]
    public class AdminController : Controller
    {
        // GET: Admin
        private kutuphaneEntities db = new kutuphaneEntities();
        public ActionResult Index()
        {
            List<KitapModel> kitapList = db.kitaplar.Select(x => new KitapModel()
            {
                KitapID=x.KitapID,
                KitapAdi = x.KitapAdi,
                KutuphanedeMi=x.KutuphanedeMi,
                Yazar = x.Yazar,
                RafNo = x.RafNo,
                BasimTarihi = x.BasimTarihi
            }).ToList();
            return View(kitapList);
        }

        public ActionResult Uyeler()
        {
            List<UyeModel> uyeList = db.kullanicilar.Where(w => w.Yetki == 0 || w.Yetki == 1).Select(x => new UyeModel
            {
                KullaniciID = x.KullaniciID,
                Adi=x.Adi,
                Soyadi=x.Soyadi,
                EPosta=x.EPosta,
                Yetki=x.Yetki
            }).ToList();
            return View(uyeList);
        }

        [HttpPost]
        public string Devredisi(int id)
        {
            try
            {
                kullanicilar k = db.kullanicilar.Find(id);

                k.Yetki = 0;

                db.Entry(k).State = EntityState.Modified;
                db.SaveChanges();

                return "1";
            }
            catch (Exception)
            {

                return "0";
            }
            

        }

        [HttpPost]
        public string Aktiflestir(int id)
        {
            try
            {
                kullanicilar k = db.kullanicilar.Find(id);

                k.Yetki = 1;

                db.Entry(k).State = EntityState.Modified;
                db.SaveChanges();

                return "1";
            }
            catch (Exception)
            {

                return "0";
            }


        }

        public ActionResult KitapEkle()
        {
            return View();
        }

        [HttpPost]
        public ActionResult KitapEkle(kitaplar kitap)
        {
            if (ModelState.IsValid)
            {
                kitap.KutuphanedeMi = 1;
                db.kitaplar.Add(kitap);
                db.SaveChanges();
                ViewBag.mesaj = "Başarıyla kaydedildi";
            }
            ViewBag.mesaj = "Kaydedilemedi";
            return View();
        }

        public ActionResult KitapGuncelle(int id)
        {
            kitaplar k = db.kitaplar.Find(id);
            ViewBag.kitap = k;
            return View();

        }

        [HttpPost]
        public ActionResult KitapGuncelle(kitaplar kitap)
        {
            if(ModelState.IsValid)
            {
                db.Entry(kitap).State = EntityState.Modified;
                db.SaveChanges();
            }
            ViewBag.kitap = db.kitaplar.Find(kitap.KitapID);
            return View();
        }

        [HttpPost]
        public string KitapSil(int id)
        {
            try
            {
                var kitap = db.kitaplar.Find(id);
                db.kitaplar.Remove(kitap);
                db.SaveChanges();
                return "1";

            }
            catch
            {
                return "-1";
            }
        }

        [HttpGet]
        public List<KitapModel> KitapAra(string id)
        {
            List<KitapModel> kitapList = db.kitaplar.Where(w => w.KitapAdi.Contains(id)).Select(x => new KitapModel
            {
                KitapID=x.KitapID,
                KitapAdi = x.KitapAdi,
                Yazar = x.Yazar,
                RafNo = x.RafNo,
                BasimTarihi = x.BasimTarihi
            }).ToList();
            if (kitapList.Count != 0)
                return kitapList;
            return null;
        }

        public ActionResult LogOff()
        {
            //Session.Remove("loginUser");
            Session.Clear();
            return Redirect("/AdminLogin");
        }
    }
}