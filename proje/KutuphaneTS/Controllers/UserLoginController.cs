﻿using KutuphaneTS.Models;
using KutuphaneTS.Models.DB;
using KutuphaneTS.Models.SessionControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace KutuphaneTS.Controllers
{
    [UserLoginControl]
    public class UserLoginController : Controller
    {
        // GET: Login

        private kutuphaneEntities db = new kutuphaneEntities();
        public ActionResult Index()
        {
            return View();
        }

        
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "EPosta,Sifre,Adi")] kullanicilar kullanici)
        {
            if (ModelState.IsValid)
            {
                kullanicilar k = db.kullanicilar.FirstOrDefault(o => o.EPosta == kullanici.EPosta && o.Sifre==kullanici.Sifre && o.Yetki==1);

                if(k != null)
                {
                    //FormsAuthentication.SetAuthCookie
                    Session.Add("adi", k.Adi);
                    Session.Add("soyadi", k.Soyadi);
                    Session.Add("userID", k.KullaniciID);
                    Session.Add("loginUser", "giris");
                    return RedirectToAction("Index", "User");
                }

                return View();
            }
            else
            {
                return View();
            }
        }

        public ActionResult Kayit()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Kayit([Bind(Include = "EPosta,Sifre,Adi,Soyadi")] kullanicilar kullanici)
        {
            if (ModelState.IsValid)
            {
                if (db.kullanicilar.FirstOrDefault(k => k.EPosta == kullanici.EPosta) == null)
                {
                    kullanici.Yetki = 1;
                    db.kullanicilar.Add(kullanici);
                    db.SaveChanges();
                    ViewBag.mesaj = "Başarıyla kayıt oldunuz";
                    return RedirectToAction("Index");
                }



                ViewBag.mesaj = "Başka bir Kullanıcı Adı ile deneyiniz";

                return View();
            }

            ViewBag.mesaj = "Bir hata oluştu";
            return View();
        }

    }
}