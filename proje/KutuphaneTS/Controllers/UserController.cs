﻿using KutuphaneTS.Models.DB;
using KutuphaneTS.Models.ORM;
using KutuphaneTS.Models.SessionControl;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KutuphaneTS.Controllers
{
    [UserControl]    
    public class UserController : Controller
    {
        private kutuphaneEntities db = new kutuphaneEntities();
        // GET: User
        public ActionResult Index()
        {
            List<KitapModel> kitapList = db.kitaplar.Select(x => new KitapModel()
            {
                KitapID = x.KitapID,
                KitapAdi = x.KitapAdi,
                Yazar = x.Yazar,
                RafNo = x.RafNo,
                KutuphanedeMi=x.KutuphanedeMi,
                BasimTarihi = x.BasimTarihi
            }).ToList();
            return View(kitapList);
        }

        public ActionResult OduncAlinanlar(int id)
        {
            ViewBag.alinanlar = db.alinankitaplar.Where(x=>x.KullaniciID==id).ToList();
            return View(db.kitaplar.ToList());

        }

        public ActionResult LogOff()
        {
            //Session.Remove("loginUser");
            Session.Clear();
            return Redirect("/UserLogin");
        }

        [HttpPost]
        public string Delete(string id)
        {
            try
            {
                var kitap = db.kitaplar.FirstOrDefault(d => d.KitapAdi == id);
                db.kitaplar.Remove(kitap);
                db.SaveChanges();
                return "1";

            }
            catch
            {
                return "-1";
            }
        }

        public ActionResult Iade(int kitapid)
        {
            alinankitaplar a = db.alinankitaplar.FirstOrDefault(x=>x.KitapID==kitapid);
            db.alinankitaplar.Remove(a);

            kitaplar k = db.kitaplar.Find(kitapid);
            k.KutuphanedeMi = 1;
            db.Entry(k).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("OduncAlinanlar/"+a.KullaniciID,"User");
        }

        
        public ActionResult OduncAl(int kitapid,int kullaniciid)
        {




            alinankitaplar a = new alinankitaplar()
            {
                KitapID = kitapid,
                KullaniciID = kullaniciid,
                AlinmaTarihi = DateTime.Now

            };
            db.alinankitaplar.Add(a);
            db.SaveChanges();

            kitaplar k = db.kitaplar.Find(kitapid);
            k.KutuphanedeMi = 0;
            db.Entry(k).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index", "User");


        }


    }
}