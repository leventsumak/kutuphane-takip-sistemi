﻿using KutuphaneTS.Models.DB;
using KutuphaneTS.Models.SessionControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KutuphaneTS.Controllers
{
    [AdminLoginControl]
    public class AdminLoginController : Controller
    {
        // GET: AdminLogin
        private kutuphaneEntities db = new kutuphaneEntities();
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "EPosta,Sifre")] kullanicilar kullanici)
        {
            if (ModelState.IsValid)
            {
                kullanicilar k = db.kullanicilar.FirstOrDefault(o => o.EPosta == kullanici.EPosta && o.Sifre == kullanici.Sifre && o.Yetki == 2);

                if (k != null)
                {
                    //FormsAuthentication.SetAuthCookie
                    Session.Add("adi", k.Adi);
                    Session.Add("soyadi", k.Soyadi);
                    Session.Add("loginAdmin", "giris");
                    return RedirectToAction("Index", "Admin");
                }

                return View();
            }
            else
            {
                return View();
            }
        }
    }
}